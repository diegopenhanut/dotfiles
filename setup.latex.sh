#!/usr/bin/env bash

# minimum install
sudo apt-get install --no-install-recommends texlive-latex-extra
sudo apt install texlive-fonts-recommended

# try those if it did not work
# sudo apt-get install lmodern
#sudo apt install texlive-plain-extra
